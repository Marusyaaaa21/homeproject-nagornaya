import Vue from 'vue'
import App from './App.vue'
import Vuelidate from 'vuelidate'
import router from './router'
import store from './store'
import dateFilter from '@/filters/date.filter'
import MessagePlugin from '@/utils/message.plugin'
import 'materialize-css/dist/js/materialize.min'
import './registerServiceWorker'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.config.productionTip = false

Vue.use(MessagePlugin)
Vue.filter('date', dateFilter)
Vue.use(Vuelidate)

firebase.initializeApp({
  apiKey: "AIzaSyD_KBxvuBbVpW4rRWp17DpmsaDdA34boJY",
  authDomain: "homeproject-nagornaya.firebaseapp.com",
  databaseURL: "https://homeproject-nagornaya.firebaseio.com",
  projectId: "homeproject-nagornaya",
  storageBucket: "homeproject-nagornaya.appspot.com",
  messagingSenderId: "268793876172",
  appId: "1:268793876172:web:f8643a590495f6bc6afc69",
  measurementId: "G-PWQSY10W4D"
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App)
    }).$mount('#app')
  }
})
